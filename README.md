# `math_utils`

> Math utilities

- Concrete data types building on types from
  [`vek`](https://github.com/yoanlcq/vek) crate
- Abstract algebraic trait hierarchy for generic $n \in \{2,3,4\}$-dimensional
  spaces
- Coordinate and transformation types with tagged units
- Strongly typed value constraints (e.g. non-negative scalars, non-zero vectors,
  invertible matrices)
- Support for fixed precision scalars from the
  [`fixed`](https://gitlab.com/tspiteri/fixed) crate
- Geometry primitives and intersection tests
