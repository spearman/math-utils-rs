use either::Either;
use fixed::types::I16F16 as Ftype;
use num_traits::One;
use serde_json;

use math_utils::*;

#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{}:\n{:?}", stringify!($e), $e); }
}

#[allow(unused_macros)]
macro_rules! display {
  ($e:expr) => { println!("{}:\n{}", stringify!($e), $e); }
}

#[derive(Clone, Copy, Debug)]
struct World;
type WorldSpace       = coordinate::Cartesian3 <Ftype, World>;
type WorldPoint       = <WorldSpace as AffineSpace <Ftype>>::Point;
type WorldVector      = <WorldSpace as AffineSpace <Ftype>>::Vector;
type WorldEndo        = <WorldVector as Module <Ftype>>::LinearEndo;
type WorldProjective  = coordinate::Displacement4 <Ftype, World>;

#[derive(Clone, Copy, Debug)]
struct Local;
type LocalSpace       = coordinate::Cartesian3 <Ftype, Local>;
type LocalVector      = <LocalSpace as AffineSpace <Ftype>>::Vector;

type WorldToLocal     = coordinate::Transform3 <Ftype, World, Local>;

fn main() {
  println!("math-utils example main...");

  report_sizes();

  println!("i32...");
  let p1 : Point2 <i32> = [1, -1].into();
  let p2 : Point2 <i32> = [-1, 1].into();
  display!(p1);
  display!(p2);
  display!(p2 - p1);
  println!("...i32");

  println!("f32...");
  display!(1.0f32.exp());
  let v : Vector2 <f32> = [0.0, 1.0].into();
  let m : Matrix2 <f32> = Matrix2::from_col_arrays ([
    [ 0.0, -1.0],
    [ 1.0,  0.0]
  ]);
  display!(v);
  display!(m);
  display!(m * v);
  println!("...f32");

  println!("I16F16...");
  display!(Ftype::from_num (1).exp());

  let v = Vector2::<Ftype>::from_num::<i32> ([0, 1].into());
  let m = Matrix2::<Ftype>::from_num::<i32> (Matrix2::from_col_arrays ([
    [ 0, -1],
    [ 1,  0]
  ]));
  display!(v);
  display!(Vector3::<Ftype>::from (v));
  display!(v.with_z (Ftype::one()));
  display!(m);
  display!(Matrix3::from (m));
  display!(m * v);
  println!("...I16F16");

  println!("coordinates...");
  let t : AffineMap <Ftype, WorldSpace, LocalSpace, WorldToLocal> = {
    let m = WorldToLocal::from_num::<i32> (Matrix3::from_col_arrays ([
      [ 0, -1, 0 ],
      [ 1,  0, 0 ],
      [ 0,  0, 1 ]
    ]));
    let x = LocalVector::from_num::<i32> ([-1, -1, 0].into());
    display!(x);
    display!(x.norm());
    display!(x.normalize());
    AffineMap::new (m, x)
  };
  let p = WorldPoint::from (Point3::<Ftype>::from_num::<i32> ([-1, -1, 0].into()));
  display!(p);
  display!(t);
  display!(t.transform (p));

  let a : Affinity <Ftype, WorldSpace, WorldSpace, WorldEndo> = {
    let m = WorldEndo::from_num::<i32> (Matrix3::from_col_arrays ([
      [ 0, -1, 0 ],
      [ 1,  0, 0 ],
      [ 0,  0, 1 ]
    ]));
    let x = WorldVector::from_num::<i32> ([-1, -1, 0].into());
    Affinity::new (LinearIso::new (m).unwrap(), x)
  };
  let h = WorldProjective::homography (a);
  display!(a);
  display!(h);
  display!(h.transform (
    WorldProjective::homogeneous::<WorldSpace> (Either::Left (p))));
  display!(serde_json::to_string (&a).unwrap());
  println!("...coordinates");

  println!("...math-utils example main");
}
