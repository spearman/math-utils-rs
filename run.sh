#!/usr/bin/env bash

set -x
cargo run --example example --features="derive_serdes"

exit 0
