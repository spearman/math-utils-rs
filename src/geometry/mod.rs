//! Geometry utilities

pub mod intersect;
pub mod primitive;
pub mod shape;

pub use self::primitive::*;
pub use self::shape::Shape;
