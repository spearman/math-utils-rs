pub use self::simplex2::Simplex2;
pub use self::simplex3::Simplex3;

/// 2D simplex types
pub mod simplex2 {
  #[cfg(feature = "derive_serdes")]
  use serde::{Deserialize, Serialize};

  use crate::*;
  use crate::geometry::*;

  /// A $n<3$-simplex in 2-dimensional space.
  ///
  /// Individual simplex variants should fail to construct in debug builds when
  /// points are degenerate.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub enum Simplex2 <S> {
    Segment  (Segment  <S>),
    Triangle (Triangle <S>)
  }

  /// A 1-simplex or line segment in 2D space.
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// identical.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub struct Segment <S> {
    a : Point2 <S>,
    b : Point2 <S>
  }

  /// A 2-simplex or triangle in 2D space
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// colinear.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub struct Triangle <S> {
    a : Point2 <S>,
    b : Point2 <S>,
    c : Point2 <S>
  }

  impl <S : Ring> Segment <S> {
    /// Panics if the points are identical:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex2::Segment;
    /// let s = Segment::new ([0.0, 0.0].into(), [0.0, 0.0].into());
    /// ```
    pub fn new (
      a : Point2 <S>,
      b : Point2 <S>
    ) -> Self where S : std::fmt::Debug {
      assert_ne!(a, b);
      Segment { a, b }
    }
    /// Debug panic if the points are identical:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex2::Segment;
    /// let s = Segment::unchecked ([0.0, 0.0].into(), [0.0, 0.0].into());
    /// ```
    pub fn unchecked (
      a : Point2 <S>,
      b : Point2 <S>
    ) -> Self where S : std::fmt::Debug {
      debug_assert_ne!(a, b);
      Segment { a, b }
    }
    #[inline]
    pub fn numcast <T> (self) -> Option <Segment <T>> where
      S : num_traits::NumCast,
      T : num_traits::NumCast
    {
      Some (Segment {
        a: self.a.numcast()?,
        b: self.b.numcast()?
      })
    }
    #[inline]
    pub fn point_a (&self) -> &Point2 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &Point2 <S> {
      &self.b
    }
    #[inline]
    pub fn length (&self) -> S where S : Field + Sqrt {
      self.vector().norm()
    }
    #[inline]
    pub fn length2 (&self) -> S {
      self.vector().self_dot()
    }
    /// Returns `point_b() - point_a()`
    #[inline]
    pub fn vector (&self) -> Vector2 <S> {
      self.b - self.a
    }
    #[inline]
    pub fn aabb2 (&self) -> Aabb2 <S> where S : std::fmt::Debug {
      Aabb2::from_points (self.a, self.b)
    }
    #[inline]
    pub fn line2 (&self) -> Line2 <S> where S : Real {
      Line2::new (self.a, Unit2::normalize (self.vector()))
    }
    #[inline]
    pub fn intersect_aabb (&self, aabb : &Aabb2 <S>)
      -> Option <((S, Point2 <S>), (S, Point2 <S>))>
    where
      S : Real + std::fmt::Debug
    {
      intersect::continuous_segment2_aabb2 (self, aabb)
    }
    #[inline]
    pub fn intersect_sphere (&self, sphere : &Sphere2 <S>)
      -> Option <((S, Point2 <S>), (S, Point2 <S>))>
    where
      S : Real + std::fmt::Debug
    {
      intersect::continuous_segment2_sphere2 (self, sphere)
    }
  }
  impl <S : Field> Default for Segment <S> {
    /// A default simplex is arbitrarily chosen to be the simplex from -1.0 to 1.0
    /// lying on the X axis:
    ///
    /// ```
    /// # use math_utils::geometry::simplex2::Segment;
    /// assert_eq!(
    ///   Segment::default(),
    ///   Segment::new ([-1.0, 0.0].into(), [1.0, 0.0].into())
    /// );
    /// ```
    fn default() -> Self {
      Segment {
        a: [-S::one(), S::zero()].into(),
        b: [ S::one(), S::zero()].into()
      }
    }
  }

  impl <S : Field> Triangle <S> {
    /// Panics if the points are colinear:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex2::Triangle;
    /// let s = Triangle::new (
    ///   [-1.0, -1.0].into(),
    ///   [ 0.0,  0.0].into(),
    ///   [ 1.0,  1.0].into());
    /// ```
    pub fn new (
      a : Point2 <S>,
      b : Point2 <S>,
      c : Point2 <S>
    ) -> Self where S : approx::RelativeEq + std::fmt::Debug {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      assert!(!colinear_2d (&a, &b, &c));
      Triangle { a, b, c }
    }
    /// Debug panic if the points are colinear:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex2::Triangle;
    /// let s = Triangle::unchecked (
    ///   [-1.0, -1.0].into(),
    ///   [ 0.0,  0.0].into(),
    ///   [ 1.0,  1.0].into());
    /// ```
    pub fn unchecked (
      a : Point2 <S>,
      b : Point2 <S>,
      c : Point2 <S>
    ) -> Self where S : approx::RelativeEq + std::fmt::Debug {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      debug_assert!(!colinear_2d (&a, &b, &c));
      Triangle { a, b, c }
    }
    #[inline]
    pub fn point_a (&self) -> &Point2 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &Point2 <S> {
      &self.b
    }
    #[inline]
    pub fn point_c (&self) -> &Point2 <S> {
      &self.c
    }
  }
  impl <S : Field + Sqrt> Default for Triangle <S> {
    /// A default triangle is arbitrarily chosen to be the equilateral triangle
    /// with point at 1.0 on the Y axis:
    ///
    /// ```
    /// # use math_utils::approx;
    /// # use math_utils::geometry::simplex2::Triangle;
    /// use std::f64::consts::FRAC_1_SQRT_2;
    /// let s = Triangle::default();
    /// let t = Triangle::new (
    ///   [           0.0,            1.0].into(),
    ///   [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2].into(),
    ///   [ FRAC_1_SQRT_2, -FRAC_1_SQRT_2].into()
    /// );
    /// approx::assert_relative_eq!(s.point_a(), t.point_a());
    /// approx::assert_relative_eq!(s.point_b(), t.point_b());
    /// approx::assert_relative_eq!(s.point_c(), t.point_c());
    /// ```
    fn default() -> Self {
      let frac_1_sqrt_2 = S::one() / (S::one() + S::one()).sqrt();
      Triangle {
        a: [     S::zero(),       S::one()].into(),
        b: [-frac_1_sqrt_2, -frac_1_sqrt_2].into(),
        c: [ frac_1_sqrt_2, -frac_1_sqrt_2].into()
      }
    }
  }
}

/// 3D simplex types
pub mod simplex3 {
  #[cfg(feature = "derive_serdes")]
  use serde::{Deserialize, Serialize};

  use crate::*;
  use crate::geometry::*;
  /// A $n<4$-simplex in 3-dimensional space.
  ///
  /// Individual simplex variants should fail to construct in debug builds when
  /// points are degenerate.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub enum Simplex3 <S> {
    Segment     (Segment     <S>),
    Triangle    (Triangle    <S>),
    Tetrahedron (Tetrahedron <S>)
  }

  /// A 1-simplex or line segment in 3D space.
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// identical.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub struct Segment <S> {
    a : Point3 <S>,
    b : Point3 <S>
  }

  /// A 2-simplex or triangle in 3D space
  ///
  /// Creation methods should fail with a debug assertion if the points are
  /// colinear.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub struct Triangle <S> {
    a : Point3 <S>,
    b : Point3 <S>,
    c : Point3 <S>
  }

  /// A 3-simplex or tetrahedron in 3D space
  ///
  /// Creation methods will fail with a debug assertion if the points are
  /// coplanar.
  #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
  #[derive(Clone, Copy, Debug, PartialEq)]
  pub struct Tetrahedron <S> {
    a : Point3 <S>,
    b : Point3 <S>,
    c : Point3 <S>,
    d : Point3 <S>
  }

  impl <S : Ring> Segment <S> {
    /// Panics if the points are identical:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex3::Segment;
    /// let s = Segment::new ([0.0, 0.0, 1.0].into(), [0.0, 0.0, 1.0].into());
    /// ```
    pub fn new (
      a : Point3 <S>,
      b : Point3 <S>
    ) -> Self where S : std::fmt::Debug {
      assert_ne!(a, b);
      Segment { a, b }
    }
    /// Debug panic if the points are identical:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex3::Segment;
    /// let s = Segment::unchecked ([0.0, 0.0, 1.0].into(), [0.0, 0.0, 1.0].into());
    /// ```
    pub fn unchecked (
      a : Point3 <S>,
      b : Point3 <S>
    ) -> Self where S : std::fmt::Debug {
      debug_assert_ne!(a, b);
      Segment { a, b }
    }
    #[inline]
    pub fn numcast <T> (self) -> Option <Segment <T>> where
      S : num_traits::NumCast,
      T : num_traits::NumCast
    {
      Some (Segment {
        a: self.a.numcast()?,
        b: self.b.numcast()?
      })
    }
    #[inline]
    pub fn point_a (&self) -> &Point3 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &Point3 <S> {
      &self.b
    }
    #[inline]
    pub fn length (&self) -> S where S : Field + Sqrt {
      self.vector().norm()
    }
    #[inline]
    pub fn length2 (&self) -> S {
      self.vector().self_dot()
    }
    /// Returns `point_b() - point_a()`
    #[inline]
    pub fn vector (&self) -> Vector3 <S> {
      self.b - self.a
    }
    #[inline]
    pub fn aabb3 (&self) -> Aabb3 <S> where S : std::fmt::Debug {
      Aabb3::from_points (self.a, self.b)
    }
    #[inline]
    pub fn line3 (&self) -> Line3 <S> where S : Real {
      Line3::new (self.a, Unit3::normalize (self.vector()))
    }
    #[inline]
    pub fn intersect_aabb (&self, aabb : &Aabb3 <S>)
      -> Option <((S, Point3 <S>), (S, Point3 <S>))>
    where
      S : Real + num_traits::Float + approx::RelativeEq <Epsilon=S> +
        std::fmt::Debug
    {
      intersect::continuous_segment3_aabb3 (self, aabb)
    }
    #[inline]
    pub fn intersect_sphere (&self, sphere : &Sphere3 <S>)
      -> Option <((S, Point3 <S>), (S, Point3 <S>))>
    where
      S : Field + Sqrt
    {
      intersect::continuous_segment3_sphere3 (self, sphere)
    }
    #[inline]
    pub fn intersect_cylinder (&self, sphere : &Cylinder3 <S>)
      -> Option <((S, Point3 <S>), (S, Point3 <S>))>
    where
      S : Real + std::fmt::Debug
    {
      intersect::continuous_segment3_cylinder3 (self, sphere)
    }
    #[inline]
    pub fn intersect_capsule (&self, capsule : &Capsule3 <S>)
      -> Option <((S, Point3 <S>), (S, Point3 <S>))>
    where
      S : Real + std::fmt::Debug
    {
      intersect::continuous_segment3_capsule3 (self, capsule)
    }
  }
  impl <S : Field> Default for Segment <S> {
    /// A default simplex is arbitrarily chosen to be the simplex from -1.0 to 1.0
    /// lying on the X axis:
    ///
    /// ```
    /// # use math_utils::geometry::simplex3::Segment;
    /// assert_eq!(
    ///   Segment::default(),
    ///   Segment::new ([-1.0, 0.0, 0.0].into(), [1.0, 0.0, 0.0].into())
    /// );
    /// ```
    fn default() -> Self {
      Segment {
        a: [-S::one(), S::zero(), S::zero()].into(),
        b: [ S::one(), S::zero(), S::zero()].into()
      }
    }
  }

  impl <S : Ring> Triangle <S> {
    /// Panics if the points are colinear:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex3::Triangle;
    /// let s = Triangle::new (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 0.0,  0.0,  0.0].into(),
    ///   [ 1.0,  1.0,  1.0].into());
    /// ```
    pub fn new (
      a : Point3 <S>,
      b : Point3 <S>,
      c : Point3 <S>
    ) -> Self where
      S : Field + Sqrt + approx::RelativeEq + std::fmt::Debug
    {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      assert!(!colinear_3d (&a, &b, &c));
      Triangle { a, b, c }
    }
    /// Debug panic if the points are colinear:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex3::Triangle;
    /// let s = Triangle::unchecked (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 0.0,  0.0,  0.0].into(),
    ///   [ 1.0,  1.0,  1.0].into());
    /// ```
    pub fn unchecked (
      a : Point3 <S>,
      b : Point3 <S>,
      c : Point3 <S>
    ) -> Self where
      S : Field + Sqrt + approx::RelativeEq + std::fmt::Debug
    {
      debug_assert_ne!(a, b);
      debug_assert_ne!(a, c);
      debug_assert_ne!(b, c);
      debug_assert!(!colinear_3d (&a, &b, &c));
      Triangle { a, b, c }
    }
    #[inline]
    pub fn point_a (&self) -> &Point3 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &Point3 <S> {
      &self.b
    }
    #[inline]
    pub fn point_c (&self) -> &Point3 <S> {
      &self.c
    }
  }
  impl <S : Field + Sqrt> Default for Triangle <S> {
    /// A default simplex is arbitrarily chosen to be the simplex on the XY plane
    /// formed by an equilateral triangle with point at 1.0 on the Y axis:
    ///
    /// ```
    /// # use math_utils::approx;
    /// # use math_utils::geometry::simplex3::Triangle;
    /// # use math_utils::*;
    /// use std::f64::consts::FRAC_1_SQRT_2;
    /// let s = Triangle::default();
    /// let t = Triangle::new (
    ///   [           0.0,            1.0, 0.0].into(),
    ///   [-FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into(),
    ///   [ FRAC_1_SQRT_2, -FRAC_1_SQRT_2, 0.0].into()
    /// );
    /// approx::assert_relative_eq!(
    ///   Matrix3::from_col_arrays ([
    ///     s.point_a().0.into_array(),
    ///     s.point_b().0.into_array(),
    ///     s.point_c().0.into_array()
    ///   ]),
    ///   Matrix3::from_col_arrays ([
    ///     t.point_a().0.into_array(),
    ///     t.point_b().0.into_array(),
    ///     t.point_c().0.into_array()
    ///   ]));
    /// ```
    fn default() -> Self {
      let frac_1_sqrt_2 = S::one() / (S::one() + S::one()).sqrt();
      Triangle {
        a: [     S::zero(),       S::one(), S::zero()].into(),
        b: [-frac_1_sqrt_2, -frac_1_sqrt_2, S::zero()].into(),
        c: [ frac_1_sqrt_2, -frac_1_sqrt_2, S::zero()].into()
      }
    }
  }

  impl <S : Ring> Tetrahedron <S> {
    /// Panics if the points are coplanar:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex3::Tetrahedron;
    /// let s = Tetrahedron::new (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 1.0,  1.0,  1.0].into(),
    ///   [-1.0,  1.0,  0.0].into(),
    ///   [ 1.0, -1.0,  0.0].into());
    /// ```
    pub fn new (
      a : Point3 <S>,
      b : Point3 <S>,
      c : Point3 <S>,
      d : Point3 <S>
    ) -> Self where
      S : approx::RelativeEq
    {
      assert!(!coplanar_3d (&a, &b, &c, &d));
      Tetrahedron { a, b, c, d }
    }
    /// Debug panic if the points are coplanar:
    ///
    /// ```should_panic
    /// # use math_utils::geometry::simplex3::Tetrahedron;
    /// let s = Tetrahedron::unchecked (
    ///   [-1.0, -1.0, -1.0].into(),
    ///   [ 1.0,  1.0,  1.0].into(),
    ///   [-1.0,  1.0,  0.0].into(),
    ///   [ 1.0, -1.0,  0.0].into());
    /// ```
    pub fn unchecked (
      a : Point3 <S>,
      b : Point3 <S>,
      c : Point3 <S>,
      d : Point3 <S>
    ) -> Self where
      S : approx::RelativeEq
    {
      debug_assert!(!coplanar_3d (&a, &b, &c, &d));
      Tetrahedron { a, b, c, d }
    }
    #[inline]
    pub fn point_a (&self) -> &Point3 <S> {
      &self.a
    }
    #[inline]
    pub fn point_b (&self) -> &Point3 <S> {
      &self.b
    }
    #[inline]
    pub fn point_c (&self) -> &Point3 <S> {
      &self.c
    }
    #[inline]
    pub fn point_d (&self) -> &Point3 <S> {
      &self.d
    }
  }
  impl <S : Field + Sqrt> Default for Tetrahedron <S> {
    /// A default simplex is arbitrarily chosen to be the simplex with vertices at
    /// unit distance from the origin with A at `[0.0, 0.0, 1.0]` and B at
    /// `[0.0, sqrt(8.0/9.0), -1.0/3.0]`, and points C and D at
    /// `[ sqrt(2.0/3.0), -sqrt(2.0/9.0), -1.0/3.0]` and
    /// `[-sqrt(2.0/3.0), -sqrt(2.0/9.0), -1.0/3.0]`.
    ///
    /// ```
    /// # use math_utils::approx;
    /// # use math_utils::geometry::simplex3::Tetrahedron;
    /// # use math_utils::*;
    /// let s = Tetrahedron::default();
    /// let t = Tetrahedron::new (
    ///   [                0.0,                 0.0,      1.0].into(),
    ///   [                0.0,  f64::sqrt(8.0/9.0), -1.0/3.0].into(),
    ///   [ f64::sqrt(2.0/3.0), -f64::sqrt(2.0/9.0), -1.0/3.0].into(),
    ///   [-f64::sqrt(2.0/3.0), -f64::sqrt(2.0/9.0), -1.0/3.0].into());
    /// approx::assert_relative_eq!(
    ///   Matrix4::from_col_arrays ([
    ///     s.point_a().0.with_w (0.0).into_array(),
    ///     s.point_b().0.with_w (0.0).into_array(),
    ///     s.point_c().0.with_w (0.0).into_array(),
    ///     s.point_d().0.with_w (0.0).into_array()
    ///   ]),
    ///   Matrix4::from_col_arrays ([
    ///     t.point_a().0.with_w (0.0).into_array(),
    ///     t.point_b().0.with_w (0.0).into_array(),
    ///     t.point_c().0.with_w (0.0).into_array(),
    ///     t.point_d().0.with_w (0.0).into_array()
    ///   ]));
    /// ```
    fn default() -> Self {
      let frac_1_3 = S::one()    / S::three();
      let sqrt_2_3 = (S::two()   / S::three()).sqrt();
      let sqrt_8_9 = (S::eight() / S::nine()).sqrt();
      let sqrt_2_9 = (S::two()   / S::nine()).sqrt();
      Tetrahedron {
        a: [S::zero(), S::zero(),  S::one()].into(),
        b: [S::zero(),  sqrt_8_9, -frac_1_3].into(),
        c: [ sqrt_2_3, -sqrt_2_9, -frac_1_3].into(),
        d: [-sqrt_2_3, -sqrt_2_9, -frac_1_3].into()
      }
    }
  }
}
