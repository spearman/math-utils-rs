//! Math types and traits

#![feature(decl_macro)]

pub use vek::approx;
pub use vek::num_traits;
pub use vek as vek;

pub mod geometry;

mod fixed;
mod traits;
mod types;

pub use self::fixed::*;
pub use self::traits::*;
pub use self::types::*;

/// 0
pub const COMPONENT_INDEX_X : usize = 0;
/// 1
pub const COMPONENT_INDEX_Y : usize = 1;
/// 2
pub const COMPONENT_INDEX_Z : usize = 2;
/// 3
pub const COMPONENT_INDEX_W : usize = 3;


/// Convenience macro
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}

/// Print the sizes of some types
pub fn report_sizes() {
  use std::mem::size_of;
  println!("report sizes...");
  show!(size_of::<Vector4 <f32>>());
  show!(size_of::<Matrix2 <f32>>());
  show!(size_of::<Matrix3 <f32>>());
  show!(size_of::<Rotation2 <f32>>());
  show!(size_of::<Rotation3 <f32>>());
  println!("...report sizes");
}
