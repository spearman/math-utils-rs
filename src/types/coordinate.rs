//! Tagged coordinate types

use std::marker::PhantomData;

#[cfg(feature = "derive_serdes")]
use serde::{Deserialize, Serialize};

use crate::num_traits as num;
use crate::traits::*;
use super::*;

macro_rules! impl_dimension {
  ( $point:ident, $vector:ident, $matrix:ident, $position:ident,
    $displacement:ident,
    [$($projective_base:ident, $projective:ident, $translation_column:ident)?],
    $transform:ident, $cartesian:ident, $ndims:expr, $dimension:expr
  ) => {
    #[doc = $dimension]
    #[doc = "cartesian coordinate space tagged with units"]
    #[derive(Clone, Copy, Debug)]
    #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
    #[repr(C)]
    pub struct $cartesian <S, U> (PhantomData <(S, U)>);

    #[doc = $dimension]
    #[doc = "linear transformation between two coordinate systems tagged with units"]
    #[derive(Debug, Default, Eq, Display)]
    #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
    #[display("{}", _0)]
    #[repr(C)]
    pub struct $transform <S, U, V> (pub $matrix <S>, PhantomData <(U, V)>) where
      S : num::Zero + num::One;

    #[doc = $dimension]
    #[doc = "point coordinates tagged with unit"]
    #[derive(Debug, Default, Eq, Display)]
    #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
    #[display("{}", _0)]
    #[repr(C)]
    pub struct $position <S, U> (pub $point <S>, PhantomData <U>);

    #[doc = $dimension]
    #[doc = "linear displacement vector coordinates tagged with unit"]
    #[derive(Debug, Default, Eq, Display)]
    #[cfg_attr(feature = "derive_serdes", derive(Serialize, Deserialize))]
    #[display("{}", _0)]
    #[repr(C)]
    pub struct $displacement <S, U> (pub $vector <S>, PhantomData <U>);

    ////////////////////////////////////////////////////////////////////////////
    //  impls
    ////////////////////////////////////////////////////////////////////////////

    //
    //  CartesianN
    //
    impl <S, U> EuclideanSpace <S> for $cartesian <S, U> where S : Real { }
    impl <S, U> AffineSpace <S> for $cartesian <S, U> where S : Real {
      type Point  = $position <S, U>;
      type Vector = $displacement <S, U>;
    }

    //
    //  TransformN
    //
    impl <S, U, V> LinearMap <S, $displacement <S, U>, $displacement <S, V>> for
      $transform <S, U, V>
    where
      S : Ring
    {
      fn determinant (self) -> S {
        self.0.determinant()
      }
      fn transpose (self) -> Self {
        $transform (self.0.transpose(), PhantomData::default())
      }
    }
    impl <S, U, V> MultiplicativeMonoid for $transform <S, U, V> where
      S : Ring
    { }
    impl <S, U, V> num::One for $transform <S, U, V> where S : Ring {
      fn one() -> Self {
        Self ($matrix::one(), PhantomData::default())
      }
    }
    impl <S, U, V> std::ops::Mul <Self> for $transform <S, U, V>
      where S : Ring
    {
      type Output = Self;
      fn mul (self, rhs : Self) -> Self {
        (self.0 * rhs.0).into()
      }
    }
    impl <S, U, V> std::ops::MulAssign <Self> for $transform <S, U, V> where
      S : Ring
    {
      fn mul_assign (&mut self, rhs : Self) {
        (self.0 *= rhs.0).into()
      }
    }
    impl <S, U, V> std::ops::Mul <$displacement <S, U>> for $transform <S, U, V>
      where S : Ring
    {
      type Output = $displacement <S, V>;
      fn mul (self, rhs : $displacement <S, U>) -> $displacement <S, V> {
        (self.0 * rhs.0).into()
      }
    }
    impl <S, U, V> PartialEq for $transform <S, U, V> where
      S : PartialEq + num::Zero + num::One
    {
      fn eq (&self, other : &Self) -> bool {
        self.0 == other.0
      }
    }
    impl <S, U, V> Copy for $transform <S, U, V> where
      S : Copy + num::Zero + num::One
    { }
    impl <S, U, V> Clone for $transform <S, U, V> where
      S : Clone + num::Zero + num::One
    {
      fn clone (&self) -> Self {
        self.0.clone().into()
      }
    }
    impl <S, U, V> From <$matrix <S>> for $transform <S, U, V> where
      S : num::Zero + num::One
    {
      fn from (matrix : $matrix <S>) -> Self {
        $transform (matrix, PhantomData::default())
      }
    }

    //
    //  DisplacementN
    //
    impl <S, U> Point <$displacement <S, U>> for $displacement <S, U> where
      S : Ring
    {
      fn to_vector (self) -> $displacement <S, U> {
        self
      }
      fn from_vector (displacement : $displacement <S, U>) -> Self {
        displacement
      }
    }
    impl <S, U> InnerProductSpace <S> for $displacement <S, U> where
      $vector <S> : InnerProductSpace <S>,
      S : Field
    { }
    impl <S, U> VectorSpace <S> for $displacement <S, U> where
      $vector <S> : VectorSpace <S>,
      S : Field
    {
      fn map <F> (self, f : F) -> Self where F : FnMut (S) -> S {
        self.0.map (f).into()
      }
    }
    impl <S, U> Dot <S> for $displacement <S, U> where
      $vector <S> : Dot <S>,
      S : Ring
    {
      fn dot (self, other : Self) -> S {
        self.0.dot (other.0)
      }
    }
    impl <S, U> std::ops::Div <S> for $displacement <S, U> where
      $vector <S> : std::ops::Div <S, Output=$vector <S>>
    {
      type Output = Self;
      fn div (self, scalar : S) -> Self {
        (self.0 / scalar).into()
      }
    }
    impl <S, U> Module <S> for $displacement <S, U> where
      $vector <S> : Module <S>,
      S : Ring
    {
      type LinearEndo = $transform <S, U, U>;
    }
    impl <S, U> std::ops::Mul <S> for $displacement <S, U> where
      $vector <S> : std::ops::Mul <S, Output=$vector <S>>
    {
      type Output = Self;
      fn mul (self, scalar : S) -> Self {
        (self.0 * scalar).into()
      }
    }
    impl <S, U> GroupAction <$position <S, U>> for $displacement <S, U> where
      S : AdditiveGroup
    {
      fn action (self, position : $position <S, U>) -> $position <S, U> {
        (position.0 + self.0).into()
      }
    }
    impl <S, U> AdditiveGroup for $displacement <S, U> where
      $vector <S> : AdditiveGroup,
      S : Ring
    { }
    impl <S, U> AdditiveMonoid for $displacement <S, U> where
      $vector <S> : AdditiveGroup,
      S : Ring
    { }
    impl <S, U> Group for $displacement <S, U> where S : AdditiveGroup {
      fn identity() -> Self {
        use num::Zero;
        Self::zero()
      }
      fn operation (a : Self, b : Self) -> Self {
        a + b
      }
    }
    impl <S, U> std::ops::Add <Self> for $displacement <S, U> where
      $vector <S> : std::ops::Add <Output=$vector <S>>
    {
      type Output = Self;
      fn add (self, other : Self) -> Self {
        (self.0 + other.0).into()
      }
    }
    impl <S, U> std::ops::Add <&Self> for $displacement <S, U> where
      $vector <S> : std::ops::Add <Output=$vector <S>>,
      S : Copy
    {
      type Output = Self;
      fn add (self, other : &Self) -> Self {
        (self.0 + other.0).into()
      }
    }
    impl <S, U> std::ops::Sub <Self> for $displacement <S, U> where
      $vector <S> : std::ops::Sub <Output=$vector <S>>
    {
      type Output = Self;
      fn sub (self, other : Self) -> Self {
        (self.0 - other.0).into()
      }
    }
    impl <S, U> std::ops::Sub <&Self> for $displacement <S, U> where
      $vector <S> : std::ops::Sub <Output=$vector <S>>,
      S : Copy
    {
      type Output = Self;
      fn sub (self, other : &Self) -> Self {
        (self.0 - other.0).into()
      }
    }
    impl <S, U> std::ops::AddAssign <Self> for $displacement <S, U> where
      $vector <S> : std::ops::AddAssign
    {
      fn add_assign (&mut self, other : Self) {
        self.0 += other.0
      }
    }
    impl <S, U> std::ops::SubAssign <Self> for $displacement <S, U> where
      $vector <S> : std::ops::SubAssign
    {
      fn sub_assign (&mut self, other : Self) {
        self.0 -= other.0
      }
    }
    impl <S, U> std::ops::Neg for $displacement <S, U> where
      $vector <S> : std::ops::Neg <Output=$vector <S>>
    {
      type Output = Self;
      fn neg (self) -> Self {
        (-self.0).into()
      }
    }
    impl <S, U> num::Zero for $displacement <S, U> where S : AdditiveGroup {
      fn zero() -> Self {
        $vector::zero().into()
      }
      fn is_zero (&self) -> bool {
        self.0 == $vector::zero()
      }
    }
    impl <S, U> PartialEq for $displacement <S, U> where
      $vector <S> : PartialEq
    {
      fn eq (&self, other : &Self) -> bool {
        self.0 == other.0
      }
    }
    impl <S, U> Copy for $displacement <S, U> where S : Copy { }
    impl <S, U> Clone for $displacement <S, U> where S : Clone {
      fn clone (&self) -> Self {
        self.0.clone().into()
      }
    }
    impl <S, U> From <$vector <S>> for $displacement <S, U> {
      fn from (vector : $vector <S>) -> Self {
        $displacement (vector, PhantomData::default())
      }
    }
    impl <S, U> From<[S; $ndims]> for $displacement <S, U> {
      fn from (array : [S; $ndims]) -> Self {
        $displacement (array.into(), PhantomData::default())
      }
    }
    // projective completion
    $(
      impl <S, U> ProjectiveSpace <S, $displacement <S, U>> for
        $projective <S, U>
      where
        S : Field + std::fmt::Display
      {
        fn homography <A> (
          affinity : Affinity <S, A, A, <A::Vector as Module <S>>::LinearEndo>
        ) -> Projectivity <
          S, $displacement <S, U>, $displacement <S, U>, Self, Self,
          Self::LinearEndo
        > where
          A : AffineSpace <S, Vector=$displacement <S, U>>
        {
          let mut transform = Self::LinearEndo::from (*affinity.linear_iso);
          transform.0.cols.$translation_column =
            $projective_base::from ((affinity.translation.0, S::one()));
          Projectivity::new (
            LinearIso::<S, Self, Self, Self::LinearEndo>::new (transform)
              .unwrap()
          )
        }
        fn homogeneous <A> (point_or_vector : Either <A::Point, A::Vector>)
          -> Self
        where
          A : AffineSpace <S, Vector=$displacement <S, U>>,
          $displacement <S, U> : GroupAction <A::Point>
        {
          match point_or_vector {
            Either::Left  (point)  =>
              $projective_base::from ((point.to_vector().0, S::one())).into(),
            Either::Right (vector) =>
              $projective_base::from ((vector.0, S::zero())).into()
          }
        }
      }
    )?

    //
    //  PositionN
    //
    impl <S, U> Point <$displacement <S, U>> for $position <S, U> where
      S : Ring
    {
      fn to_vector (self) -> $displacement <S, U> {
        self.0.to_vector().into()
      }
      fn from_vector (displacement : $displacement <S, U>) -> Self {
        $point::from_vector (displacement.0).into()
      }
    }
    impl <S, U> std::ops::Sub <Self> for $position <S, U> where
      $vector <S> : std::ops::Sub <Output=$vector <S>>,
      S : AdditiveGroup
    {
      type Output = $displacement <S, U>;
      fn sub (self, other : Self) -> $displacement <S, U> {
        (self.0 - other.0).into()
      }
    }
    impl <S, U> PartialEq for $position <S, U> where
      $point <S> : PartialEq
    {
      fn eq (&self, other : &Self) -> bool {
        self.0 == other.0
      }
    }
    impl <S, U> Copy for $position <S, U> where S : Copy { }
    impl <S, U> Clone for $position <S, U> where S : Clone {
      fn clone (&self) -> Self {
        self.0.clone().into()
      }
    }
    impl <S, U> From <$point <S>> for $position <S, U> {
      fn from (point : $point <S>) -> Self {
        $position (point, PhantomData::default())
      }
    }
    impl <S, U> From<[S; $ndims]> for $position <S, U> {
      fn from (array : [S; $ndims]) -> Self {
        $position (array.into(), PhantomData::default())
      }
    }
  }
}

impl_dimension!(Point2, Vector2, Matrix2, Position2,
  Displacement2, [Vector3, Displacement3, z], Transform2, Cartesian2, 2, "2D");
impl_dimension!(Point3, Vector3, Matrix3, Position3,
  Displacement3, [Vector4, Displacement4, w], Transform3, Cartesian3, 3, "3D");
impl_dimension!(Point4, Vector4, Matrix4, Position4,
  Displacement4, [], Transform4, Cartesian4, 4, "4D");

//
//  DisplacementN
//
impl <S, U> From <Displacement2 <S, U>> for Displacement3 <S, U> where
  S : Field
{
  fn from (transform : Displacement2 <S, U>) -> Self {
    Displacement3 (transform.0.into(), PhantomData::default())
  }
}
impl <S, U> From <Displacement3 <S, U>> for Displacement4 <S, U> where
  S : Field
{
  fn from (transform : Displacement3 <S, U>) -> Self {
    Displacement4 (transform.0.into(), PhantomData::default())
  }
}

//
//  TransformN
//
impl <S, U, V> From <Transform2 <S, U, V>> for Transform3 <S, U, V> where
  S : Field
{
  fn from (transform : Transform2 <S, U, V>) -> Self {
    Transform3 (transform.0.into(), PhantomData::default())
  }
}
impl <S, U, V> From <Transform3 <S, U, V>> for Transform4 <S, U, V> where
  S : Field
{
  fn from (transform : Transform3 <S, U, V>) -> Self {
    Transform4 (transform.0.into(), PhantomData::default())
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[derive(Default)]
  struct Foo;
  #[derive(Default)]
  struct Bar;

  #[test]
  fn defaults() {
    assert_eq!(Position3::<f32, Foo>::default().0, Point3::origin());
    assert_eq!(Displacement3::<f32, Foo>::default().0, Vector3::zero());
    assert_eq!(Transform3::<f32, Foo, Bar>::default().0, Matrix3::identity());
  }
}
